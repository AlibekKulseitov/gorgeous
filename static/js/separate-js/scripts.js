//'use strict';

$(function() {

    // ------------------------------------------------------- //
    // Multi Level dropdowns
    // ------------------------------------------------------ //

    $(".dropdown-menu [data-toggle='dropdown']").on("click", function(event) {
        event.preventDefault();
        event.stopPropagation();

        $(this).siblings().toggleClass("show");

        if (!$(this).next().hasClass('show')) {
            $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        $(this).parents('.dropdown.show').on('hidden.bs.dropdown', function(e) {
            $('.dropdown-submenu .show').removeClass("show");
        });

    });

    /*
    |--------------------------------------------------------------------------
    | // https://www.coralnodes.com/hide-header-on-scroll-down/
    |--------------------------------------------------------------------------
    */

    if ($('#header').length > 0) {

        var doc = document.documentElement;
        var w = window;

        var prevScroll = w.scrollY || doc.scrollTop;
        var curScroll;
        var direction = 0;
        var prevDirection = 0;
        var headerHeight = $("#header").height();

        var header = document.getElementById('header');

        var checkScroll = function() {

            /*
            ** Find the direction of scroll
            ** 0 - initial, 1 - up, 2 - down
            */

            curScroll = w.scrollY || doc.scrollTop;
            if (curScroll > prevScroll) {
                //scrolled up
                direction = 2;
            }
            else if (curScroll < prevScroll) {
                //scrolled down
                direction = 1;
            }

            if (direction !== prevDirection) {
                toggleHeader(direction, curScroll);
            }

            prevScroll = curScroll;
        };

        var toggleHeader = function(direction, curScroll) {
            if (direction === 2 && curScroll > headerHeight) {
                header.classList.add('is-hidden');
                prevDirection = direction;
            }
            else if (direction === 1) {
                header.classList.remove('is-hidden');
                prevDirection = direction;
            }
        };

        window.addEventListener('scroll', checkScroll);

    }

   /*
   |--------------------------------------------------------------------------
   | Showing modal with effect
   |--------------------------------------------------------------------------
   */

    $('.js-modal-effect').on('click', function(e) {

        e.preventDefault();

        var effect = $(this).attr('data-effect');
        $('.modal').addClass(effect);
    });
    // hide modal with effect

    /*
    |--------------------------------------------------------------------------
    | Mobile Menu
    |--------------------------------------------------------------------------
    */

    var burger = $('.js-menu-trigger');

    burger.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        // Toggle Menu
        $('.m-menu').toggleClass('is-show');

        // Disable Scroll On Body
        if ($(this).hasClass('is-open')) {
            $('body').css({"overflow": "hidden"});
        } else {
            $('body').css({"overflow": ""});
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Category Modal
    |--------------------------------------------------------------------------
    */

    var categoryModal = $('.js-category-modal');

    categoryModal.on('click', function(e) {

        // Toggle Open Class
        $(this).toggleClass('is-open');

        // Toggle Category Modal
        $('.category-modal').toggleClass('is-show');

        // Disable Scroll On Body
        if ($(this).hasClass('is-open')) {
            $('body').css({"overflow": "hidden"});
        } else {
            $('body').css({"overflow": ""});
        }
    });

    var categoryModalClose = $('.js-category-modal-close');

    categoryModalClose.on('click', function(e) {

        // Toggle Open Class
        categoryModal.toggleClass('is-open');

        // Toggle Category Modal
        $('.category-modal').toggleClass('is-show');

        $('body').css({"overflow": ""});
    });

    /*
    |--------------------------------------------------------------------------
    | Responsive Iframe Inside Modal
    |--------------------------------------------------------------------------
    */

    function toggle_video_modal() {

        // Click on video thumbnail or link
        $(".js-video-modal").on("click", function(e){

            // prevent default behavior for a-tags, button tags, etc.
            e.preventDefault();

            // Grab the video ID from the element clicked
            var id = $(this).attr('data-youtube-id');

            // Autoplay when the modal appears
            // Note: this is intetnionally disabled on most mobile devices
            // If critical on mobile, then some alternate method is needed
            var autoplay = '?autoplay=1';

            // Don't show the 'Related Videos' view when the video ends
            var related_no = '&rel=0';

            // String the ID and param variables together
            var src = '//www.youtube.com/embed/'+id+autoplay+related_no;

            // Pass the YouTube video ID into the iframe template...
            // Set the source on the iframe to match the video ID
            $(".video-modal__iframe").attr('src', src);

            // Add class to the body to visually reveal the modal
            $("body").addClass("video-modal-show");

            $('body').css({"overflow": "hidden"});

        });

        // Close and Reset the Video Modal
        function close_video_modal() {

            event.preventDefault();

            // re-hide the video modal
            $("body").removeClass("video-modal-show");

            $('body').css({"overflow": ""});

            // reset the source attribute for the iframe template, kills the video
            $(".video-modal__iframe").attr('src', '');

        }
        // if the 'close' button/element, or the overlay are clicked
        $('body').on('click', '.video-modal__close, .video-modal__overlay', function(event) {

            // call the close and reset function
            close_video_modal();

        });
        // if the ESC key is tapped
        $('body').keyup(function(e) {
            // ESC key maps to keycode `27`
            if (e.keyCode == 27) {

                // call the close and reset function
                close_video_modal();

            }
        });
    }
    toggle_video_modal();

    /*
    |--------------------------------------------------------------------------
    | Smooth Scroll
    |--------------------------------------------------------------------------
    */

    $('.js-page-scroll').on('click', function(event) {
        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
            let target = $(this.hash),
                speed = $(this).data("speed") || 800;
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top - 0
                }, speed);
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Spoiler Text
    |--------------------------------------------------------------------------
    */

    var containerHeight = document.querySelectorAll(".js-spoiler-inner");
    var uncoverLink = document.querySelectorAll(".js-spoiler-more");

    for(let i = 0; i < containerHeight.length; i++){
        let openData = uncoverLink[i].dataset.open;
        let closeData = uncoverLink[i].dataset.close;
        let curHeight = containerHeight[i].dataset.height;

        uncoverLink[i].innerHTML = openData;
        containerHeight[i].style.maxHeight = curHeight + "px";

        uncoverLink[i].addEventListener("click", function(){
            if(containerHeight[i].classList.contains("is-open")){

                containerHeight[i].classList.remove("is-open");

                uncoverLink[i].innerHTML = openData;

                containerHeight[i].style.maxHeight = curHeight + "px";

            } else {
                containerHeight[i].removeAttribute("style");

                containerHeight[i].classList.add("is-open");

                uncoverLink[i].innerHTML = closeData;

            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Services Slider
    |--------------------------------------------------------------------------
    */

	let servicesSlider = new Swiper('.js-services-slider', {
		speed: 600,
		mousewheel: false,
		loop: true,
		spaceBetween: 30,
		navigation: {
			nextEl: '.js-services-slider-next',
			prevEl: '.js-services-slider-prev',
		},
		slidesPerView: 4,
		breakpoints: {
			1024: {
				slidesPerView: 3,
                spaceBetween: 10,
			},
			768: {
				slidesPerView: 2,
			},
			640: {
				slidesPerView: 1,
			},
			320: {
				slidesPerView: 1,
			}
		}
	});

    /*
    |--------------------------------------------------------------------------
    | Light Gallery
    |--------------------------------------------------------------------------
    */

	$('.js-lg').lightGallery({
		selector: ".js-lg-item",
	});

    /*
    |--------------------------------------------------------------------------
    | Bootstrap Tooltip
    |--------------------------------------------------------------------------
    */

    $('[data-toggle="tooltip"]').tooltip();
    // colored tooltip
    $('[data-toggle="tooltip-primary"]').tooltip({
        template: '<div class="tooltip tooltip-primary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });
    $('[data-toggle="tooltip-secondary"]').tooltip({
        template: '<div class="tooltip tooltip-secondary" role="tooltip"><div class="arrow"><\/div><div class="tooltip-inner"><\/div><\/div>'
    });

    /*
    |--------------------------------------------------------------------------
    | Clipboard.js
    |--------------------------------------------------------------------------
    */

    if ($('.clipboard-icon').length) {
        var clipboard = new ClipboardJS('.clipboard-icon');

        $('.clipboard-icon').attr('data-toggle', 'tooltip').attr('title', 'Copy to clipboard');


        $('[data-toggle="tooltip"]').tooltip();

        clipboard.on('success', function(e) {
            e.trigger.classList.value = 'clipboard-icon btn-current'
            $('.btn-current').tooltip('hide');
            e.trigger.dataset.originalTitle = 'Copied';
            $('.btn-current').tooltip('show');
            setTimeout(function(){
                $('.btn-current').tooltip('hide');
                e.trigger.dataset.originalTitle = 'Copy to clipboard';
                e.trigger.classList.value = 'clipboard-icon'
            },1000);
            e.clearSelection();
        });
    }

    /*
    |--------------------------------------------------------------------------
    | Beer Slider
    |--------------------------------------------------------------------------
    */

    $.fn.BeerSlider = function ( options ) {
        options = options || {};
        return this.each(function() {
            new BeerSlider(this, options);
        });
    };

    $('.beer-slider').BeerSlider({
        prefix: 'beer'
    });

    /*
    |--------------------------------------------------------------------------
    | Select2
    |--------------------------------------------------------------------------
    */

    $(".select2").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
        });
    });

    $(".select2-search").each(function() {
        $(this).select2({
            placeholder: $(this).attr('data-placeholder'),
            width: '100%',
            allowClear: false,
            minimumResultsForSearch: 5
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Amaze UI Datetime Picker
    | https://github.com/amazeui/datetimepicker
    |--------------------------------------------------------------------------
    */

    /**
     * Russian translation for bootstrap-datetimepicker
     * Victor Taranenko <darwin@snowdale.com>
     */
    ;(function($){
        $.fn.datetimepicker.dates['ru'] = {
            days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"],
            daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб", "Вск"],
            daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
            months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
            monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
            today: "Сегодня",
            suffix: [],
            meridiem: []
        };
    }(jQuery));

    // AmazeUI Datetimepicker
    $('.js-datetimepicker').datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        autoclose: true,
        language: 'ru'
    });

    /*
    |--------------------------------------------------------------------------
    | Back to Top
    |--------------------------------------------------------------------------
    */

    $(window).on("scroll", function(e) {
        if ($(this).scrollTop() > 0) {
            $('.js-back-to-top').fadeIn('slow');
        } else {
            $('.js-back-to-top').fadeOut('slow');
        }
    });

    $(".js-back-to-top").on("click", function(e) {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*
    |--------------------------------------------------------------------------
    | Benefits Slider
    |--------------------------------------------------------------------------
    */

    let benefitsSlider = new Swiper('.js-benefits-slider', {
        speed: 600,
        mousewheel: false,
        loop: false,
        touchRatio: 0,
        spaceBetween: 70,
        slidesPerView: 3,
        autoHeight: true,
        navigation: {
            nextEl: '.js-benefits-next',
            prevEl: '.js-benefits-prev',
        },
        pagination: {
            el: '.js-benefits-pagination',
            type: 'fraction',
        },
        breakpoints: {
            992: {
                slidesPerView: 1,
                spaceBetween: 5,
                touchRatio: 1,
            },
            1170: {
                slidesPerView: 3.2,
                spaceBetween: 30,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Gallery Slider
    |--------------------------------------------------------------------------
    */

    let gallerySlider = new Swiper('.js-gallery-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 70,
        slidesPerView: 1.8,
        centeredSlides: true,
        autoHeight: false,
        navigation: {
            nextEl: '.js-gallery-next',
            prevEl: '.js-gallery-prev',
        },
        pagination: {
            el: '.js-gallery-pagination',
            type: 'fraction',
        },
        breakpoints: {
            320: {
                slidesPerView: 1.4,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 1.4,
                spaceBetween: 20,
            },
            1170: {
                slidesPerView: 1.8,
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Compare Slider
    |--------------------------------------------------------------------------
    */

    let compareSlider = new Swiper('.js-compare-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 70,
        slidesPerView: 1.8,
        centeredSlides: true,
        autoHeight: false,
        navigation: {
            nextEl: '.js-compare-next',
            prevEl: '.js-compare-prev',
        },
        pagination: {
            el: '.js-compare-pagination',
            type: 'fraction',
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 1.4,
                spaceBetween: 20,
            },
            1170: {
                slidesPerView: 1.8,
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Zone Slider
    |--------------------------------------------------------------------------
    */

    let zoneSlider = new Swiper('.js-zone-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 30,
        slidesPerView: 5,
        autoHeight: false,
        navigation: {
            nextEl: '.js-zone-next',
            prevEl: '.js-zone-prev',
        },
        pagination: {
            el: '.js-zone-pagination',
        },
        breakpoints: {
            320: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            456: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 30,
            },
            1170: {
                slidesPerView: 4,
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Members Slider
    |--------------------------------------------------------------------------
    */

    var membersSliderConfig = {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 40,
        slidesPerView: 'auto',
        centeredSlides: true,
        autoHeight: false,
        pagination: {
            el: '.js-members-pagination',
            type: 'fraction',
        },
        breakpoints: {
            320: {
                slidesPerView: 1.4,
                spaceBetween: 20,
            },
            992: {
                spaceBetween: 20,
            },
            1170: {
                spaceBetween: 20,
            },
        }
    };

    var membersSlider = $(".js-members-slider");

    membersSlider.each(function() {
        var $membersSliderControl = $(this);
        var membersNext = $membersSliderControl.parent().find(".js-members-next");
        var membersPrev = $membersSliderControl.parent().find(".js-members-prev");
        membersSliderConfig.navigation = {
            nextEl: membersNext,
            prevEl: membersPrev,
            disabledClass: 'is-disabled',
        };
        new Swiper(this, membersSliderConfig);
    });

    /*
    |--------------------------------------------------------------------------
    | Reviews Slider
    |--------------------------------------------------------------------------
    */

    let reviewsSlider = new Swiper('.js-reviews-slider', {
        speed: 600,
        mousewheel: false,
        loop: false,
        touchRatio: 1,
        spaceBetween: 40,
        slidesPerView: 3,
        autoHeight: false,
        observer: true,
        observeParents: true,
        centeredSlides: false,
        watchSlidesVisibility: true,
        navigation: {
            nextEl: '.js-reviews-next',
            prevEl: '.js-reviews-prev',
        },
        pagination: {
            el: '.js-reviews-pagination',
            type: 'fraction',
        },
        breakpoints: {
            320: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 1.5,
                spaceBetween: 20,
            },
            768: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 1.3,
                spaceBetween: 20,
            },
            992: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1170: {
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Services Benefits Slider
    |--------------------------------------------------------------------------
    */

    let servicesBenefitsSlider = new Swiper('.js-services-benefits-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 40,
        slidesPerView: 'auto',
        centeredSlides: true,
        autoHeight: false,
        navigation: {
            nextEl: '.js-services-benefits-next',
            prevEl: '.js-services-benefits-prev',
        },
        pagination: {
            el: '.js-services-benefits-pagination',
            type: 'fraction',
        },
        breakpoints: {
            320: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 1.4,
                spaceBetween: 15,
            },
            768: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 1.5,
                spaceBetween: 15,
            },
            992: {
                centeredSlides: true,
                loop: true,
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1170: {
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Products Slider
    |--------------------------------------------------------------------------
    */

    const productsSlider = $(".js-products-slider");

    const productsSliderConfig = {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 30,
        slidesPerView: 3,
        autoHeight: false,
        breakpoints: {
            320: {
                slidesPerView: 2.1,
                spaceBetween: 5,
            },
            768: {
                slidesPerView: 2.05,
                spaceBetween: 10,
            },
            992: {
                slidesPerView: 2.4,
                spaceBetween: 20,
            },
            1170: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }
    };


    productsSlider.each(function(index) {
        var $productsSliderControl = $(this);

        var $productsSliderNext = $productsSliderControl.parent().find(".js-products-slider-next");
        var $productsSliderPrev = $productsSliderControl.parent().find(".js-products-slider-prev");

        $productsSliderNext.attr('id', 'js-products-slider-next-' + index);
        $productsSliderPrev.attr('id', 'js-products-slider-prev-' + index);

        productsSliderConfig.navigation = {
            nextEl: '#js-products-slider-next-' + index,
            prevEl: '#js-products-slider-prev-' + index
        };

        new Swiper(this, productsSliderConfig);
    });

    /*
    |--------------------------------------------------------------------------
    | Brands Slider
    |--------------------------------------------------------------------------
    */

    let brandsSlider = new Swiper('.js-brands-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 30,
        slidesPerView: 4,
        autoHeight: false,
        navigation: {
            nextEl: '.js-brands-slider-next',
            prevEl: '.js-brands-slider-prev',
        },
        breakpoints: {
            320: {
                slidesPerView: 2.4,
                spaceBetween: 15,
            },
            768: {
                slidesPerView: 2.4,
                spaceBetween: 15,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1170: {
                spaceBetween: 20,
            },
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Products Slider2
    |--------------------------------------------------------------------------
    */

    let productsSlider2 = new Swiper('.js-products-slider2', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 30,
        slidesPerView: 4,
        navigation: {
            nextEl: '.js-products-slider2-next',
            prevEl: '.js-products-slider2-prev',
        },
        autoHeight: false,
        breakpoints: {
            320: {
                slidesPerView: 2.1,
                spaceBetween: 5,
            },
            768: {
                slidesPerView: 2.05,
                spaceBetween: 10,
            },
            992: {
                slidesPerView: 2.4,
                spaceBetween: 20,
            },
            1170: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        }
    });

   /*
   |--------------------------------------------------------------------------
   | Product Slider
   |--------------------------------------------------------------------------
   */

    let productSlider = new Swiper('.js-product-slider', {
        speed: 600,
        mousewheel: false,
        loop: true,
        touchRatio: 1,
        spaceBetween: 5,
        slidesPerView: 1,
        autoHeight: false,
        navigation: {
            nextEl: '.js-product-gallery-next',
            prevEl: '.js-product-gallery-prev',
        },
        thumbs: {
            swiper: {
                el: '.js-product-thumbs',
                spaceBetween: 20,
                slidesPerView: 4,
                speed: 700,
                touchRatio: 0,
                watchSlidesVisibility: true,
                watchSlidesProgress: true,
                breakpoints: {
                    320: {
                        spaceBetween: 10,
                    },
                    640: {
                        spaceBetween: 10,
                    },
                    768: {
                        spaceBetween: 20,
                    },
                    992: {
                        spaceBetween: 20,
                    },

                    1200: {
                        spaceBetween: 20,
                    },
                }
            }
        }
    });

    /*
    |--------------------------------------------------------------------------
    | Filters Modal
    |--------------------------------------------------------------------------
    */

    $(document).ready(function() {
        Modal.init();
    });

    var Modal = {

        effects : [
            'fade',
            'scale',
            'newspaper',
            'slide-in-top',
            'slide-in-left',
            'slide-in-right',
            'slide-in-bottom',
            'expand-horiz',
            'expand-vert',
            'sticky-top',
            'sticky-bottom',
            'rotate-top',
            'rotate-bottom',
            'elastic-flip',
            'bounce-zoom'
        ],

        init: function() {
            $('.js-filters-trigger').on('click', function(e) {
                e.stopPropagation();
                e.preventDefault();

                $('#effect-name').html($(this).text());

                var effect = $(this).attr('data-effect');
                $('.filters').addClass(effect).addClass('is-show').removeClass('is-hiding');
                $('body').addClass('modal-open');
                $('body').addClass('filters-' + effect);
            });

            $('.filters__overlay').on('click', function(e) {
                $('.filters').removeClass('is-show').addClass('is-hiding');

                setTimeout(function() {
                    for(var i = 0; i < Modal.effects.length; i++) {
                        $('.filters').removeClass(Modal.effects[i]);
                        $('body').removeClass('filters-' + Modal.effects[i]);
                    }

                    $('.filters').removeClass('is-hiding');
                    $('body').removeClass('modal-open');
                }, 250);
            });

            $('.js-filters-close').on('click', function(e) {
                $('.filters').removeClass('is-show').addClass('is-hiding');

                setTimeout(function() {
                    for(var i = 0; i < Modal.effects.length; i++) {
                        $('.filters').removeClass(Modal.effects[i]);
                        $('body').removeClass('filters-' + Modal.effects[i]);
                    }

                    $('.filters').removeClass('is-hiding');
                    $('body').removeClass('modal-open');
                }, 250);
            });
        }
    };

});
